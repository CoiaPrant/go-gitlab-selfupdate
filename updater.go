package selfupdate

import (
	"context"
	"fmt"
	"regexp"

	gitlab "github.com/xanzy/go-gitlab"
)

// Updater is responsible for managing the context of self-update.
// It contains Gitlab client and its context.
type Updater struct {
	api       *gitlab.Client
	apiCtx    context.Context
	validator Validator
	filters   []*regexp.Regexp
}

// Config represents the configuration of self-update.
type Config struct {
	// APIToken represents Gitlab API token. If it's not empty, it will be used for authentication of Gitlab API
	APIToken string
	// SelfHostedBaseURL is a base URL of Gitlab API. If you want to use this library with Gitlab self-hosted,
	// please set "https://{your-organization-address}/api/v4/" to this field.
	SelfHostedBaseURL string
	// Validator represents types which enable additional validation of downloaded release.
	Validator Validator
	// Filters are regexp used to filter on specific assets for releases with multiple assets.
	// An asset is selected if it matches any of those, in addition to the regular tag, os, arch, extensions.
	// Please make sure that your filter(s) uniquely match an asset.
	Filters []string
}

// NewUpdater creates a new updater instance. It initializes Gitlab API client.
// If you set your API token to $GITLAB_TOKEN, the client will use it.
func NewUpdater(config Config) (*Updater, error) {
	token := config.APIToken
	ctx := context.Background()
	filtersRe := make([]*regexp.Regexp, 0, len(config.Filters))
	for _, filter := range config.Filters {
		re, err := regexp.Compile(filter)
		if err != nil {
			return nil, fmt.Errorf("Could not compile regular expression %q for filtering releases: %v", filter, err)
		}
		filtersRe = append(filtersRe, re)
	}

	if config.SelfHostedBaseURL == "" {
		client, _ := gitlab.NewClient(token)
		return &Updater{api: client, apiCtx: ctx, validator: config.Validator, filters: filtersRe}, nil
	}

	client, err := gitlab.NewClient(token, gitlab.WithBaseURL(config.SelfHostedBaseURL))
	if err != nil {
		return nil, err
	}
	return &Updater{api: client, apiCtx: ctx, validator: config.Validator, filters: filtersRe}, nil
}

// DefaultUpdater creates a new updater instance with default configuration.
// It initializes Gitlab API client with default API base URL.
// If you set your API token to $GITLAB_TOKEN, the client will use it.
func DefaultUpdater() *Updater {
	ctx := context.Background()
	client, _ := gitlab.NewClient("")
	return &Updater{api: client, apiCtx: ctx}
}
