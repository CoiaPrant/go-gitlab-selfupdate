package selfupdate

import (
	"fmt"
	"regexp"
	"runtime"
	"strings"

	"github.com/blang/semver"
	gitlab "github.com/xanzy/go-gitlab"
	"golang.org/x/sys/cpu"
)

var reVersion = regexp.MustCompile(`\d+\.\d+\.\d+`)

func findAssetFromRelease(rel *gitlab.Release,
	prefixes, suffixes []string, targetVersion string, filters []*regexp.Regexp) (*gitlab.ReleaseLink, semver.Version, bool) {

	if targetVersion != "" && targetVersion != rel.TagName {
		log.Println("Skip", rel.TagName, "not matching to specified version", targetVersion)
		return nil, semver.Version{}, false
	}

	verText := rel.TagName
	indices := reVersion.FindStringIndex(verText)
	if indices == nil {
		log.Println("Skip version not adopting semver", verText)
		return nil, semver.Version{}, false
	}
	if indices[0] > 0 {
		log.Println("Strip prefix of version", verText[:indices[0]], "from", verText)
		verText = verText[indices[0]:]
	}

	// If semver cannot parse the version text, it means that the text is not adopting
	// the semantic versioning. So it should be skipped.
	ver, err := semver.Make(verText)
	if err != nil {
		log.Println("Failed to parse a semantic version", verText)
		return nil, semver.Version{}, false
	}

	for _, asset := range rel.Assets.Links {
		name := asset.Name
		if len(filters) > 0 {
			// if some filters are defined, match them: if any one matches, the asset is selected
			matched := false
			for _, filter := range filters {
				if filter.MatchString(name) {
					log.Println("Selected filtered asset", name)
					matched = true
					break
				}
				log.Printf("Skipping asset %q not matching filter %v\n", name, filter)
			}
			if !matched {
				continue
			}
		}

		if len(prefixes) == 0 {
			for _, s := range suffixes {
				if strings.HasSuffix(name, s) { // require version, arch etc
					// default: assume single artifact
					return asset, ver, true
				}
			}
		} else {
			for _, p := range prefixes {
				for _, s := range suffixes {
					if strings.HasPrefix(name, p) && strings.HasSuffix(name, s) { // require version, arch etc
						// default: assume single artifact
						return asset, ver, true
					}
				}
			}
		}
	}

	log.Println("No suitable asset was found in release", rel.TagName)
	return nil, semver.Version{}, false
}

func findValidationAsset(rel *gitlab.Release, validationName string) (*gitlab.ReleaseLink, bool) {
	for _, asset := range rel.Assets.Links {
		if asset.Name == validationName {
			return asset, true
		}
	}
	return nil, false
}

func findReleaseAndAsset(rels []*gitlab.Release, binaryName, targetVersion string, filters []*regexp.Regexp) (*gitlab.Release, *gitlab.ReleaseLink, semver.Version, bool) {
	// Generate candidates
	prefixes := make([]string, 0, 2)
	suffixes := make([]string, 0, 2*8*3)
	for _, sep := range []rune{'_', '-'} {
		for _, ext := range []string{".zip", ".tar.gz", ".tgz", ".gzip", ".gz", ".tar.xz", ".xz", ""} {
			suffix := fmt.Sprintf("%s%c%s%s", runtime.GOOS, sep, runtime.GOARCH, ext)
			suffixes = append(suffixes, suffix)

			suffix = fmt.Sprintf("%s%c%s%s%s", runtime.GOOS, sep, runtime.GOARCH, getArchExt(), ext)
			suffixes = append(suffixes, suffix)
			if runtime.GOOS == "windows" {
				suffix = fmt.Sprintf("%s%c%s.exe%s", runtime.GOOS, sep, runtime.GOARCH, ext)
				suffixes = append(suffixes, suffix)
			}
		}

		if binaryName != "" {
			prefix := fmt.Sprintf("%s%c", binaryName, sep)
			prefixes = append(prefixes, prefix)
		}
	}

	var ver semver.Version
	var asset *gitlab.ReleaseLink
	var release *gitlab.Release

	// Find the latest version from the list of releases.
	// Returned list from Gitlab API is in the order of the date when created.
	//   ref: https://gitlab.com/p14yground/go-gitlab-selfupdate/issues/11
	for _, rel := range rels {
		if a, v, ok := findAssetFromRelease(rel, prefixes, suffixes, targetVersion, filters); ok {
			// Note: any version with suffix is less than any version without suffix.
			// e.g. 0.0.1 > 0.0.1-beta
			if release == nil || v.GTE(ver) {
				ver = v
				asset = a
				release = rel
			}
		}
	}

	if release == nil {
		log.Println("Could not find any release for", runtime.GOOS, "and", runtime.GOARCH)
		return nil, nil, semver.Version{}, false
	}

	return release, asset, ver, true
}

// DetectLatest tries to get the latest version of the repository on Gitlab. 'slug' means 'owner/name' formatted string.
// It fetches releases information from Gitlab API and find out the latest release with matching the tag names and asset names.
// Drafts and pre-releases are ignored. Assets would be suffixed by the OS name and the arch name such as 'foo_linux_amd64'
// where 'foo' is a command name. '-' can also be used as a separator. File can be compressed with zip, gzip, zxip, tar&zip or tar&zxip.
// So the asset can have a file extension for the corresponding compression format such as '.zip'.
// On Windows, '.exe' also can be contained such as 'foo_windows_amd64.exe.zip'.
func (up *Updater) DetectLatest(slug, binaryName string) (release *Release, found bool, err error) {
	return up.DetectVersion(slug, binaryName, "")
}

// DetectVersion tries to get the given version of the repository on Gitlab. `slug` means `owner/name` formatted string.
// And version indicates the required version.
func (up *Updater) DetectVersion(slug string, binaryName, version string) (release *Release, found bool, _ error) {
	rels, res, err := up.api.Releases.ListReleases(slug, nil, gitlab.WithContext(up.apiCtx))
	if err != nil {
		log.Println("API returned an error response:", err)
		if res != nil && res.StatusCode == 404 {
			// 404 means repository not found or release not found. It's not an error here.
			err = nil
			log.Println("API returned 404. Repository or release not found")
		}
		return nil, false, err
	}

	rel, asset, ver, found := findReleaseAndAsset(rels, binaryName, version, up.filters)
	if !found {
		return nil, false, nil
	}

	url := asset.DirectAssetURL
	log.Println("Successfully fetched the latest release. tag:", rel.TagName, ", name:", rel.Name, ", Asset:", url)

	publishedAt := rel.CreatedAt
	release = &Release{
		Version:           ver,
		AssetURL:          url,
		AssetID:           asset.ID,
		ValidationAssetID: -1,
		ReleaseNotes:      rel.Description,
		Name:              rel.Name,
		PublishedAt:       publishedAt,
		RepoOwner:         strings.Split(slug, "/")[0],
		RepoName:          strings.Split(slug, "/")[1],
	}

	if up.validator != nil {
		validationName := asset.Name + up.validator.Suffix()
		validationAsset, ok := findValidationAsset(rel, validationName)
		if !ok {
			return nil, false, fmt.Errorf("Failed finding validation file %q", validationName)
		}
		release.ValidationAssetID = validationAsset.ID
	}

	return release, true, nil
}

// DetectLatest detects the latest release of the slug (owner/repo).
// This function is a shortcut version of updater.DetectLatest() method.
func DetectLatest(slug, binaryName string) (*Release, bool, error) {
	return DefaultUpdater().DetectLatest(slug, binaryName)
}

// DetectVersion detects the given release of the slug (owner/repo) from its version.
func DetectVersion(slug string, binaryName, version string) (*Release, bool, error) {
	return DefaultUpdater().DetectVersion(slug, version, binaryName)
}

func getArchExt() string {
	switch runtime.GOARCH {
	case "arm":
		if cpu.ARM.HasVFPv3 || cpu.ARM.HasVFPv3D16 || cpu.ARM.HasVFPv4 {
			return "v7"
		} else if cpu.ARM.HasVFP || cpu.ARM.HasVFPD32 {
			return "v6"
		} else {
			return "v5"
		}

	case "amd64":
		if cpu.X86.HasAVX512 && cpu.X86.HasAVX512F && cpu.X86.HasAVX512BW && cpu.X86.HasAVX512CD && cpu.X86.HasAVX512DQ && cpu.X86.HasAVX512VL {
			return "v4"
		} else if cpu.X86.HasAVX && cpu.X86.HasAVX2 && cpu.X86.HasBMI1 && cpu.X86.HasBMI2 && cpu.X86.HasFMA && cpu.X86.HasOSXSAVE {
			return "v3"
		} else if cpu.X86.HasPOPCNT && cpu.X86.HasSSE3 && cpu.X86.HasSSE41 && cpu.X86.HasSSE42 && cpu.X86.HasSSSE3 {
			return "v2"
		} else {
			return "v1"
		}
	}

	return ""
}
